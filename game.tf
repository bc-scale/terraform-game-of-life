variable "seed" {
  type = string
  # Gosper glider gun
  default = <<EOL
                        *
                      * *
            **      **            **
           *   *    **            **
**        *     *   **
**        *   * **    * *
          *     *       *
           *   *
            **
EOL
  #     # Glider
  #     default = <<EOL
  #   *
  # * *
  #  **
  # EOL
}

variable "new_game" {
  type    = bool
  default = false
}

data "terraform_remote_state" "self" {
  count = var.new_game ? 0 : 1


  backend = "local"

  config = {
    path = "terraform.tfstate"
  }
}

locals {
  previous_map = (length(data.terraform_remote_state.self) == 1 ?
    data.terraform_remote_state.self[0].outputs["map"] : # either use previous state if we have it
  var.seed)                                              # or start with the seedd

  lines  = split("\n", local.previous_map)
  height = length(local.lines)
  width = max([
    for line in local.lines : length(line)
  ]...) # this syntax requires terraform 0.13

  # previous_active is a set of "y,x" pairs which have life
  # this is useful for more efficient neighbour lookups
  previous_active = toset(flatten([
    for y, line in local.lines : [
      for x, c in split("", line) :
      "${y},${x}" if c == "*"
    ]
  ]))

  # computes all eight neighbour positions
  neighbour_directions = flatten([
    for y in range(-1, 2) : [
      for x in range(-1, 2) : {
        x = x,
        y = y,
      } if x != 0 || y != 0
    ]
  ])

  # for all the tiles in the grid + 1 tile on either side to allow for expansion
  # calculate number of previously active neighbours
  new_neighbours = flatten([
    for y in range(-1, local.height + 1) : [
      for x in range(-1, local.width + 1) : {
        x          = x,
        y          = y,
        was_active = contains(local.previous_active, "${y},${x}")
        count = length([
          for neighbour_dir in local.neighbour_directions :
          1 if contains(local.previous_active, "${y + neighbour_dir.y},${x + neighbour_dir.x}")
        ])
      }
    ]
  ])

  # filters out dead cells from new_neighbours
  new_active = [
    for neighbours in local.new_neighbours :
    neighbours
    if(neighbours.was_active && neighbours.count == 2) || neighbours.count == 3
  ]

  # converts new_active to set, to allow for lookups when rendering the map to string
  new_active_set = toset([
    for active in local.new_active : "${active.y},${active.x}"
  ])

  # calculates the bounds of the resulting map
  # in this simulation, the window moves (or expands) together with the simulation
  min_x = min([for pos in local.new_active : pos.x]...)
  max_x = max([for pos in local.new_active : pos.x]...)
  min_y = min([for pos in local.new_active : pos.y]...)
  max_y = max([for pos in local.new_active : pos.y]...)

  # render the map to string, line by line, by constructing lists and then joining them
  # to create the result.
  new_map = join("\n", [
    for y in range(local.min_y, local.max_y + 1) :
    join(
      "",
      [
        for x in range(local.min_x, local.max_x + 1) :
        contains(local.new_active_set, "${y},${x}") ? "*" : " "
      ]
    )

  ])
}

output "map" {
  value = local.new_map
}

# uncomment for debugging

# output "previous_active" {
#     value = local.previous_active
# }

# output "neighbour_directions" {
#     value = local.neighbour_directions
# }

# output "new_neighbours" {
#     value = local.new_neighbours
# }

# output "new_active" {
#     value = local.new_active
# }